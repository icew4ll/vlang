import net.http
import x.json2

fn main() {
	// url := 'https://api.coingecko.com/api/v3/ping'
	url := 'https://api.coingecko.com/api/v3/coins'
	res := http.get(url) or { panic(err) }
	mut result := json2.raw_decode(res.text) ?
	println(result)
	// data := result.as_map()
	// links := data['organic'].arr()
	// for link in links {
	// 	url := link.as_map(['url'].str())
	// 	print(url)
	// }
}
