import os { symlink, exec, rm, is_link, mkdir_all, rmdir_all, is_dir, base, args, home_dir }
import term { green, yellow, red }

fn main() {
	if args.len < 2 {
		println(red('Argument not supplied'))
	} else {
		match args[1] {
			'sys' { sys() }
			'vlang' { build(Cfg{'https://github.com/vlang/v', 'make', 'v'}) }
			'nvim' { build(Cfg{'https://github.com/neovim/neovim', 'make CMAKE_BUILD_TYPE=Release CMAKE_INSTALL_PREFIX=. install', 'build/bin/nvim'}) }
			'sumneko' { build(Cfg{'https://github.com/sumneko/lua-language-server', 'git submodule update --init --recursive && cd 3rd/luamake && ninja -f ninja/linux.ninja && cd ../.. && ./3rd/luamake/luamake rebuild', 'bin/Linux/lua-language-server'}) }
			'nnn' { build(Cfg{'https://github.com/jarun/nnn', 'make 0_NERD=1 O_PCRE=1 && rsync -a plugins/ $home_dir()/.config/nnn/plugins', 'nnn'}) }
			'tmux' { build(Cfg{'https://github.com/tmux/tmux', 'sh autogen.sh && ./configure && make', 'tmux'}) }
			'font' { build(Cfg{'https://github.com/fontforge/fontforge', 'mkdir build && cd build && cmake -GNinja .. && ninja', 'build/bin/fontforge'}) }
			else { print('command not found') }
		}
	}
}

struct Env {
	url  string
	name string
	dest string
	bin  string
	link string
}

struct Cfg {
	url  string
	cmds string
	bin  string
}

fn sys() {
	pkgs := [
		'libgc-dev',
		'libjpeg-dev',
		'libtiff5-dev',
		'libpng-dev',
		'libfreetype6-dev',
		'libgif-dev',
		'libgtk-3-dev',
		'libxml2-dev',
		'libpango1.0-dev',
		'libcairo2-dev',
		'libspiro-dev',
		'libuninameslist-dev',
		'python3-dev',
		'ninja-build',
		'cmake',
		'build-essential',
		'gettext',
	].join(' ')
	cmds := 'echo sudo apt install $pkgs'
	cmder(cmds)
}

fn cmder(cmds string) {
	results := exec(cmds) or { panic(err) }
	println('$results.output')
}

fn build(c Cfg) {
	url := c.url
	dir := home_dir() + '/bin'
	name := base(c.bin)
	build := '$dir/build'
	dest := '$build/$name'
	link := '$dir/$name'
	bin := '$dest/$c.bin'
	cmds := 'git clone $c.url $dest && cd $dest && $c.cmds'
	e := Env{url, name, dest, bin, link}
	println(yellow('INSTALLING $name ...'))
	println(e)
	println(green(cmds))
	if is_dir(dest) {
		rmdir_all(dest)
	}
	if !is_dir(build) {
		mkdir_all(build)
	}
	cmder(cmds)
	if is_link(link) {
		rm(link)
	}
	symlink(bin, link) or { panic(err) }
	println(green('$name INSTALL COMPLETE'))
}
