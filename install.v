import os

fn main() {
	url := 'https://github.com/vlang/v'
	bin := os.home_dir() + '/bin'
	name := os.base(url)
	build := '$bin/build'
	dest := '$build/$name'
	link := '$bin/$name'
	if os.is_dir(dest) {
		println('$dest is dir')
		os.rmdir_all(dest)
	}
	if !os.is_dir(build) {
		os.mkdir_all(build)
	}
	results := os.exec('git clone $url $dest && cd $dest && make') or { panic(err) }
	println('$results.output')
	if !os.is_link(link) {
		os.symlink('$dest/v', link) or { panic(err) }
	}
}
