import os

fn main() {
	url := 'https://github.com/vlang/v'
	bin := os.home_dir() + '/bin'
	name := os.base(url)
	build := '$bin/build'
	dest := '$build/$name'
	link := '$bin/$name'

	if !os.is_dir(build) {
		println('$build is not a dir')
	} else {
		println('$build is a dir')
	}

	if os.is_link(link) {
		println('$link is a link')
	} else {
		println('$link is not a link')
	}
}
